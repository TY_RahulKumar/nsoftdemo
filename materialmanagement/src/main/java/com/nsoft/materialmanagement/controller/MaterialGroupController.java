package com.nsoft.materialmanagement.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttribute;

import com.nsoft.materialmanagement.dto.MaterialGroupCreation;
import com.nsoft.materialmanagement.dto.UserBean;
import com.nsoft.materialmanagement.service.MaterialGroupService;

@Controller
public class MaterialGroupController {

	@Autowired
	private MaterialGroupService service;

	@GetMapping("/login")
	public String getLogin() {
		
		return "LoginData";
	}// end of getLogin

	@PostMapping("/header")
	public String authenticate(int id, String password, HttpServletRequest request, ModelMap map) {

		UserBean userBean = service.authenticate(id, password);

		if (userBean != null) {
			HttpSession session = request.getSession(true);
			session.setAttribute("user", userBean);
			return "header";
		} else {
			map.addAttribute("errmsg", "Invalid Credentials");
			return "LoginData";
		}
	}// end of authentication

	@GetMapping("/header")
	public String headerPage() {

		return "header";
	}// end of headerPage()

	@GetMapping("/showAdd")
	public String addForm(@SessionAttribute(name = "user", required = false) UserBean bean, ModelMap map) {
		if (bean != null) {
			return "materialGroup/addmaterialgroup";
		} else {
			map.addAttribute("errmsg", "Please Login First");
			return "LoginData";
		}

	}// end of addForm()

	@PostMapping("/showAdd")
	public String addMaterialGroup(@SessionAttribute(name = "user", required = false) UserBean bean, UserBean empData,
			ModelMap map, MaterialGroupCreation creation) throws InterruptedException {

		if (bean != null) {

			if (service.addMaterialGroup(bean.getId(), creation)) {
				map.addAttribute("msg", " Successfully added");
			} else {
				map.addAttribute("msg", "Not Successfully added");
			}
			Thread.sleep(2000);
			return "materialGroup/addmaterialgroup";

		} else {
			map.addAttribute("errmsg", "Please Login First");
			return "LoginData";
		}

	}// end of addMaterialGroup()

	@GetMapping("/deletematerialgroup")
	public String deleteForm(@SessionAttribute(name = "user", required = false) UserBean bean, ModelMap map) {
		if (bean != null) {
			return "materialGroup/deleteMaterialGroup";
		} else {
			map.addAttribute("errmsg", "Please Login First");
			return "LoginData";
		}

	}// end of deleteForm()

	@GetMapping("/delete")
	public String deleteMaterialGroup(String materialGroupCode, String materialGroupName,
			@SessionAttribute(name = "user", required = false) UserBean bean, ModelMap map)
			throws InterruptedException {

		if (bean != null) {

			if (service.deleteMaterialGroup(materialGroupName, materialGroupCode)) {
				map.addAttribute("msg", "Delete Successfully");

			} else {
				map.addAttribute("msg", "Data not Found for materialGroupCode :" + materialGroupCode);
			}
			Thread.sleep(2000);
			return "materialGroup/deleteMaterialGroup";

		} else {
			map.addAttribute("errmsg", "Please Login First");
			return "LoginData";
		}
	}// end of deleteMaterialGroup()

	@GetMapping("/showUpdate")
	public String updateForm(@SessionAttribute(name = "user", required = false) UserBean bean, ModelMap map) {
		
		if (bean != null) {
			map.addAttribute("id", bean.getId());
			return "materialGroup/materialgroupupdate";
		} else {
			map.addAttribute("errmsg", "Please Login First");
			return "LoginData";
		}

	}// end of updateForm()

	@PostMapping("/showUpdate")
	public String updateMaterialGroupData(@SessionAttribute(name = "user", required = false) UserBean bean,
			ModelMap map, String materialGroupCode, String materialGroupName, HttpServletRequest request)
			throws InterruptedException {

		if (bean != null) {

			if (service.updateMaterialGroup(bean.getId(), materialGroupName, materialGroupCode)) {
				map.addAttribute("msg", "Updated Succesfully");
			} else {
				map.addAttribute("msg", "Data Not found");
			}
			Thread.sleep(2000);
			return "materialGroup/materialgroupupdate";

		} else {
			map.addAttribute("errmsg", "Please Login First");
			return "Login";
		}
	}// end of updateMaterialGroupData()

	@RequestMapping(value = "getAll/{materialGroupName}", method = RequestMethod.GET)
	public @ResponseBody List<MaterialGroupCreation> getMaterialGroupCode(@PathVariable String materialGroupName) {

		List<MaterialGroupCreation> list = service.getMaterialGroup(materialGroupName);

		return list;
	}// end of getMaterialGroupCode()

	@RequestMapping(value = "/getAll", method = RequestMethod.GET)
	public @ResponseBody List<MaterialGroupCreation> getAllData() {

		List<MaterialGroupCreation> list = service.getAllMaterialGroup();

		return list;
	}// end of getAllData()

	@GetMapping("/logout")
	public String logout(HttpSession session, ModelMap map) {
		session.invalidate();
		map.addAttribute("errmsg", "Logout SuccessFully");
		return "LoginData";

	}// end of logout()

}// end of Controller class

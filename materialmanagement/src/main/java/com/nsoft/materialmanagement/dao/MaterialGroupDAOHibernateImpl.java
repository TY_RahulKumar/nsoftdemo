package com.nsoft.materialmanagement.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.nsoft.materialmanagement.dto.MaterialGroupCreation;
import com.nsoft.materialmanagement.dto.UserBean;

@Repository
public class MaterialGroupDAOHibernateImpl implements MaterialGroupDAO {

	@PersistenceUnit
	private EntityManagerFactory factory;

	@Override
	public UserBean authenticate(int id, String pwd) {

		EntityManager manager = null;

		try {

			manager = factory.createEntityManager();

			UserBean bean = manager.find(UserBean.class, id);

			if (bean != null) {
				if (bean.getPassword().equals(pwd)) {
					return bean;
				} else {
					return null;
				}
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {

			if (manager != null) {
				manager.close();
			}
		}
	}// End of authenticate(int,String)

	@Override
	public List<MaterialGroupCreation> getMaterialGroup(String materialGroupName) {

		EntityManager manager = null;

		try {

			manager = factory.createEntityManager();

			String jpql = " from MaterialGroupCreation where materialGroupName=:mn";

			Query query = manager.createQuery(jpql);

			query.setParameter("mn", materialGroupName);

			List<MaterialGroupCreation> list = query.getResultList();

			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (manager != null) {
				manager.close();
			}
		}

	}// End of getMaterialGroup(int)

	@Override
	public boolean deleteMaterialGroup(String materialGroupName, String materialGroupCode) {

		EntityManager manager = null;
		EntityTransaction transaction = null;

		try {

			manager = factory.createEntityManager();
			transaction = manager.getTransaction();

			transaction.begin();

			String jpql = "Delete from MaterialGroupCreation m where m.materialGroupName=:mn and m.materialGroupCode=:mc";

			Query query = manager.createQuery(jpql);

			query.setParameter("mn", materialGroupName);
			query.setParameter("mc", materialGroupCode);

			query.executeUpdate();

			transaction.commit();

			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {

			if (manager != null) {
				manager.close();
			}
		}
	}// End of deleteMaterialGroup(String,String)

	@Override
	public boolean addMaterialGroup(int id, MaterialGroupCreation materialGroupCreation) {

		EntityManager manager = null;
		EntityTransaction transaction = null;

		try {

			manager = factory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			MaterialGroupCreation creation = new MaterialGroupCreation();
			creation.setMaterialGroupName(materialGroupCreation.getMaterialGroupName());
			creation.setMaterialGroupCode(materialGroupCreation.getMaterialGroupCode());
			creation.setCreatedBy(id);
			creation.setCreatedDate(new Date());
			creation.setLastModifiedBy(id);
			creation.setLastModifiedDate(new Date());
			creation.setDeleted(false);
			creation.setRemarks(null);

			manager.persist(creation);

			transaction.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {

			if (manager != null) {
				manager.close();
			}
		}

	}// end of addMaterialGroup(int,MaterialGroupCreation)

	@Override
	public boolean updateMaterialGroup(int id, String materialGroupName, String materialGroupCode) {

		EntityManager manager = null;
		EntityTransaction transaction = null;

		try {
			manager = factory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();

			String jpql = "Update MaterialGroupCreation e SET e.materialGroupCode=:mc,e.lastModifiedBy=:lm,e.lastModifiedDate=:ld where e.materialGroupName=:mn";

			Query query = manager.createQuery(jpql);

			query.setParameter("mc", materialGroupCode);
			query.setParameter("lm", id);
			query.setParameter("ld", new Date());
			query.setParameter("mn", materialGroupName);
			query.executeUpdate();

			transaction.commit();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {

			if (manager != null) {
				manager.close();
			}
		}

	}// End of updateMaterialGroup(MaterialGroupCreation)

	@Override
	public List<MaterialGroupCreation> getAllMaterialGroup() {

		EntityManager manager = null;

		try {

			manager = factory.createEntityManager();

			String jpql = " from MaterialGroupCreation";

			Query query = manager.createQuery(jpql);

			List<MaterialGroupCreation> list = query.getResultList();

			return list;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			if (manager != null) {
				manager.close();
			}
		}
	}// end of getAllMaterialGroup()

}// End of DAO Class
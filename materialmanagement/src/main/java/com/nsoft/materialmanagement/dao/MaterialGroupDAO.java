package com.nsoft.materialmanagement.dao;

import java.util.List;

import com.nsoft.materialmanagement.dto.MaterialGroupCreation;
import com.nsoft.materialmanagement.dto.UserBean;

public interface MaterialGroupDAO {

	public UserBean authenticate(int id, String pwd);

	public List<MaterialGroupCreation> getMaterialGroup(String materialGroupName);
	
	public List<MaterialGroupCreation> getAllMaterialGroup();

	public boolean deleteMaterialGroup(String materialGroupName, String materialGroupCode);

	public boolean addMaterialGroup(int id,MaterialGroupCreation materialGroupCreation);

	public boolean updateMaterialGroup(int id,String materialGroupName, String materialGroupCode);

}// End of Interface
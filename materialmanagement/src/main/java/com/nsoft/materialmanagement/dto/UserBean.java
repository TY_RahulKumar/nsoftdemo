package com.nsoft.materialmanagement.dto;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.Data;
@Data
@Entity
@Table 
public class UserBean implements Serializable{
	@Id
	@Column
	private int id;
	
	@Column
	private String password;
	
}

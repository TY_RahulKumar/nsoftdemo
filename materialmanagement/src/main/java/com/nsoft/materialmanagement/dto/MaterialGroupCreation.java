package com.nsoft.materialmanagement.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data

@Entity
@Table
public class MaterialGroupCreation implements Serializable {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int materialGroupId;

	@Column(name = "MaterialGroupName")
	private String materialGroupName;

	@Column(name = "MaterialGroupCode")
	private String materialGroupCode;

	@Column(name = "CreatedBy")
	private int createdBy;

	@Column(name = "CreatedDate")
	private Date createdDate;

	@Column(name = "IsDeleted")
	private boolean isDeleted;

	@Column(name = "LastModifiedBy")
	private int lastModifiedBy;

	@Column(name = "LastModifiedDate")
	private Date lastModifiedDate;

	@Column(name = "Remarks")
	private String remarks;

}// End of class

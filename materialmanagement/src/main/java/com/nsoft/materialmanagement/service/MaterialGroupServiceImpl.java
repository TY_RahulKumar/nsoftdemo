package com.nsoft.materialmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nsoft.materialmanagement.dao.MaterialGroupDAO;
import com.nsoft.materialmanagement.dto.MaterialGroupCreation;
import com.nsoft.materialmanagement.dto.UserBean;

@Service
public class MaterialGroupServiceImpl implements MaterialGroupService {

	@Autowired
	private MaterialGroupDAO dao;

	@Override
	public UserBean authenticate(int id, String pwd) {

		return dao.authenticate(id, pwd);

	}// End of authenticate(int,String)

	@Override
	public List<MaterialGroupCreation> getMaterialGroup(String materialGroupName) {

		return dao.getMaterialGroup(materialGroupName);

	}// End of getMaterialGroup(int)

	@Override
	public boolean deleteMaterialGroup(String materialGroupName, String materialGroupCode) {

		return dao.deleteMaterialGroup(materialGroupName, materialGroupCode);

	}// End of delMaterialGroup(int)

	@Override
	public boolean addMaterialGroup(int id, MaterialGroupCreation materialGroupCreation) {

		return dao.addMaterialGroup(id, materialGroupCreation);

	}// End of addMaterialGroup(MaterialGroupCreation)

	@Override
	public boolean updateMaterialGroup(int id, String materialGroupName, String materialGroupCode) {

		return dao.updateMaterialGroup(id,  materialGroupName,  materialGroupCode);

	}// End of updateMaterialGroup(MaterialGroupCreation)

	@Override
	public List<MaterialGroupCreation> getAllMaterialGroup() {

		return dao.getAllMaterialGroup();
		
	}// End of getAllMaterialGroup 

}// End of Service Class
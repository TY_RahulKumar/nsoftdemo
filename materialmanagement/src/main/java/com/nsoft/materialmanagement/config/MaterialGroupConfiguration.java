package com.nsoft.materialmanagement.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalEntityManagerFactoryBean;

@Configuration
public class MaterialGroupConfiguration {

	@Bean
	public LocalEntityManagerFactoryBean getEntityManagerFactory() {

		LocalEntityManagerFactoryBean bean = new LocalEntityManagerFactoryBean();
		bean.setPersistenceUnitName("demo");

		return bean;

	}// End of getEntityManagerFactory()

}// End of Configuration Class

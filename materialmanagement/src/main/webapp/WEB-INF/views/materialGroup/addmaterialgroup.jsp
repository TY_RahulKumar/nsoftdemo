<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ include file="../header.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>Add Material Group</title>

<meta charset="ISO-8859-1">

<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>


<spring:url value="/resources/css/materialGroup.css" var="mainCss" />
<spring:url value="/resources/js/materialGroup/addmaterialgroup.js"
	var="mainJs" />

<link href="${mainCss}" rel="stylesheet" />
<script src="${mainJs}"></script>



</head>
<body>
	<div class="col-md-6 offset-md-4 "
		style="margin-top: 150px; display: inline-block; margin-left: 500px">
		
		<form name="myform" action="./showAdd" method="post"
			autocomplete="off">
			<div class="center">
				<div class="card ">
					<div class="card-header"
						style="background-color: #114e60; color: white;">Add
						Material Group Creation</div>
					<br />
					<div class="card-body login ">

						<div class="form-group row ">
							<label for="materialGroupName" class="col-md-4 col-form-label">Material
								Group Name:</label>
							<div class="col-sm-8">
								<input type="text" class="form-select col-md-10 inp mt-1"
									aria-label="Default select example" id="materialGroupName"
									name="materialGroupName" placeholder="Material Group Name"
									onkeyup="sendInfo()">
							</div>
						</div>


						<div class="form-group row">
							<label for="materialGroupCode" class="col-md-4 col-form-label">Material
								Group Code:</label>
							<div class="col-sm-8">
								<input type="text" class="form-select col-md-10 inp mt-1"
									aria-label="Default select example" id="materialGroupCode"
									name="materialGroupCode" onkeyup="checkMatrialCode()"
									placeholder="Material Group Code">
							</div>
						</div>
						<br>

						<div class="card-footer"
							style="padding-left: 40px; margin-top: 8px; background-color: white;">
							<button type="button" class="btn btn-primary" method="get"
								action="./showAdd">Add</button>

							<a href="./deletematerialgroup" class="card-link"
								style="padding-left: 60px; color: blue;">Delete</a> <a
								class="card-link" href="./showUpdate"
								style="padding-left: 40px; color: blue;">Update</a>
							<button id="myBtn" type="button" class="card-link"
								style="margin-left: 50px; color: blue; border: none; background-color: white; background: none; padding: 0">
								<input type="submit" value="Submit" class="btn"	onclick="login()" style="color: blue" />
							</button>

							<a href="./header" class="card-link"
								style="padding-left: 30px; color: blue;">Exit</a>
						</div>
					</div>
				</div>
			</div>

		</form>




		<div id="myModal" class="modal">
			<div class=" row">
				<div class="alert-box" style="float: none; margin: 0 auto;">
					<div class="alert alert-success">
						<div class="alert-icon text-center">
							<i class="fa fa-check-square-o  fa-3x" aria-hidden="true"></i>
						</div>
						<div class="alert-message text-center">
							<strong>Successfully added</strong>
						</div>
					</div>
				</div>
			</div>
		</div>


	</div>




	<script>
		// Get the modal
		var modal = document.getElementById("myModal");

		// Get the button that opens the modal
		var btn = document.getElementById("myBtn");

		/* // When the user clicks the button, open the modal 
		btn.onclick = function myfunction() {

		} */

		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
				modal.style.display = "none";
			}
		}
	</script>



</body>
</html>
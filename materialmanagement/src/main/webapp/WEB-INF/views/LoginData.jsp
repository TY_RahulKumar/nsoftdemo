<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
String msg = (String) request.getAttribute("errmsg");
%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Login page</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link rel="icon" type="image/png" sizes="32x32"
	href="./images/favicon-32x32.png">



<style type="text/css">
@import
	url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap')
	;

* {
	box-sizing: border-box;
}

body {
	background-color: #f3f4ed;
	background-image: url(images/bg-intro-desktop.png);
	font-family: 'Poppins', sans-serif;
	color: #fff;;
}

.container {
	display: flex;
	align-items: center;
	justify-content: center;
	flex-wrap: wrap;
	margin: 0 auto;
	max-width: 1200px;
	padding: 20px;
	min-height: 100vh;
}

.container>div {
	flex: 1;
	padding: 0 20px;
}

h1 {
	font-size: 40px;
	line-height: 50px;
	max-width: 500px;
	color: #114e60;
	text-align: center
}

p {
	opacity: 0.8;
	font-size: 15px;
	max-width: 500px;
}

.box {
	background-color: #fff;
	padding: 20px;
	box-shadow: 0 6px rgba(0, 0, 0, 0.2);
	border-radius: 8px;
	margin-bottom: 20px;
	text-align: center;
}

.box p {
	margin: 0;
}

.box-blue {
	background-color: #114e60;
	padding: 15px;
}

.form-control {
	margin-bottom: 18px;
	position: relative;
}

.form-control small {
	text-align: right;
	opacity: 0;
}

input {
	border-radius: 5px;
	border: 1px solid hsl(246, 25%, 77%);
	font-family: 'Poppins', sans-serif;
	font-size: 14px;
	font-weight: 500;
	padding: 15px 25px;
	display: block;
	width: 100%;
	outline: none;
	color: hsl(249, 10%, 26%);
}

.form-control.error input {
	border-color: hsl(0, 100%, 74%);
	color: hsl(0, 100%, 74%);
}

.form-control.error input::placeholder {
	color: hsl(0, 100%, 74%);
}

input:focus {
	border: 1px solid hsl(249, 10%, 26%);
}

.form-control img {
	position: absolute;
	top: 16px;
	right: 20px;
	opacity: 0;
	height: 20px;
}

.form-control.error img {
	opacity: 1;
}

.button {
	background-color: #114e60;
	border: 1px solid hsl(246, 25%, 77%);
	border-radius: 5px;
	color: #fff;
	font-family: 'Poppins', sans-serif;
	font-size: 14px;
	font-weight: 500;
	padding: 15px 25px;
	display: block;
	width: 100%;
	box-shadow: 0 2px rgba(0, 0, 0, 0.2);
	cursor: pointer;
	transition: transform 0.2s ease;
	text-transform: uppercase;
	letter-spacing: 1px;
	margin-bottom: 5px;
}

button:focus {
	outline: none;
}

button:active {
	border-bottom-width: 1px;
	transform: translateY(2px);
}

small {
	color: hsl(0, 100%, 74%);
	font-weight: 600;
	display: block;
	font-size: 10px;
	margin-bottom: 15px;
}

.term {
	color: hsl(246, 25%, 77%);
}

a {
	text-decoration: none;
	color: hsl(0, 100%, 74%);
	font-weight: 600;
}

@media screen and (max-width:768px) {
	h1 {
		font-size: 30px;
		line-height: 40px;
		margin-top: 50px;
		text-align: center;
	}
	p {
		text-align: center;
		margin-bottom: 40px;
	}
	.container {
		flex-direction: column;
	}
	button {
		margin-bottom: 5px;
	}
}
</style>
</head>
<body>

	<div class="container">
		<div>
			<h1>
				Material Management <span> System</span>
			</h1>
		</div>

		<div>
			<%
			if (msg != null && !msg.isEmpty()) {
			%>
			<h5 style="color: red; text-align: center">
				<%=msg%>
			</h5>
			<%
			}
			%>
			<div class="box box-blue">
				<p>
					<strong>Login Form</strong>
				</p>
			</div>
			<form class="box" id="form" action="./header" method="post"
				autocomplete="off">
				<div class="form-control">
					<input type="text" id="id" placeholder="User Id" name="id" /> <small></small>
				</div>

				<div class="form-control">
					<input type="password" id="password" placeholder="Password"
						name="password" /> <small></small>
				</div>

				<input type="submit" class="button" value="log In"> <small
					class="term">By clicking the button, you are agreeing to
					our <a href="#">Terms and Services</a>
				</small>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		const form =document.getElementById('form');
		

		form.addEventListener('submit', e => {
		   
			
		 
		   const email=form['id'].value;
		   const password=form['password'].value;



		   if(email === ''){
			  
		       addErrorTo('id','User Id is Required.');
		       e.preventDefault();
		   }else{
		       removeErrorForm('id')
		   }

		   if(password === ''){
			   
		       addErrorTo('password','Password is Required.');
		       e.preventDefault();
		   }else{
		       removeErrorForm('password')
		   }
		 

		});

		function addErrorTo(field,message){
		   const formControl=form[field].parentNode;
		   formControl.classList.add('error');


		   const small =formControl.querySelector('small');
		   small.innerText=message;
		   small.style.opacity='1';
		}

		function removeErrorForm(field){
		   const formControl=form[field].parentNode;
		   formControl.classList.remove('error');

		   const small =formControl.querySelector('small');
		   small.style.opacity='0';

		}
	</script>

</body>
</html>
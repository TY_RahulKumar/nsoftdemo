<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous">

<style>
body {
	background-color: #f3f4ed;
}

.btn-spacing {
	margin-right: -100px;
	margin-bottom: 5px !important;
	margin-left: 150px;
}

.btn-sml {
	padding: 5px 5px;
	font-size: 5px;
	border-radius: 2px;
}

#butonHover:hover {
	background-color: #2155D9;
}

#cardheader {
	background-color: #151414;
}

#anchorclr:hover {
	color: white;
}

.vertical-nav {
	min-width: 17rem;
	width: 17rem;
	height: 100vh;
	position: fixed;
	top: 0;
	left: 0;
	box-shadow: 3px 3px 10px rgba(0, 0, 0, 0.1);
	transition: all 0.4s;
}

.page-content {
	width: calc(100% - 17rem);
	margin-left: 17rem;
	transition: all 0.4s;
}

/* for toggle behavior */
#sidebar.active {
	margin-left: -17rem;
}

#content.active {
	width: 100%;
	margin: 0;
}

@media ( max-width : 768px) {
	#sidebar {
		margin-left: -17rem;
	}
	#sidebar.active {
		margin-left: 0;
	}
	#content {
		width: 100%;
		margin: 0;
	}
	#content.active {
		margin-left: 17rem;
		width: calc(100% - 17rem);
	}
}

body {
	min-height: 100vh;
	overflow-x: hidden;
}

.separator {
	margin: 3rem 0;
	border-bottom: 1px dashed #fff;
}

.text-uppercase {
	letter-spacing: 0.1em;
}

.text-gray {
	color: #aaa;
}

.modal {
	display: none; /* Hidden by default */
	position: fixed; /* Stay in place */
	z-index: 1; /* Sit on top */
	padding-top: 100px; /* Location of the box */
	left: 0;
	top: 0;
	width: 100%; /* Full width */
	height: 100%; /* Full height */
	overflow: auto; /* Enable scroll if needed */
	background-color: rgb(0, 0, 0); /* Fallback color */
	background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
}

/* /* The Close Button */
.close {
	color: #aaaaaa;
	float: right;
	font-size: 28px;
	font-weight: bold;
}

.close:hover, .close:focus {
	color: #000;
	text-decoration: none;
	cursor: pointer;
}

.alert-box {
	max-width: 300px;
	min-height: 300px;
}

.alert-icon {
	padding-bottom: 20px;
}

.input {
	border-top: none;
	border-left: none;
	border-right: none;
}

.inputField1 {
	margin-left: 50px;
	margin-top: 2%;
}

.inputField2 {
	margin-left: 50px;
	margin-top: 2%;
	padding-bottom: 4%;
}
</style>
</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark  "
		style="background-color: #114e60;">
		<a class="navbar-brand " href="" style="font-size: 25px">Logo</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent"
			style="padding-left: 1000px">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active" style="color: white; font-size: 25px">
				</li>
				<li class="nav-item"
					style="padding-left: 20px; color: white; font-size: 25px"></li>
				<li class="nav-item dropdown"
					style="padding-left: 20px; color: white; font-size: 25px"></li>
				<li class="nav-item"></li>
			</ul>
			<form class="form-inline my-2 my-lg-0" action="./logout" method="get">
				<button class="btn btn-secondary" type="submit">Logout</button>
			</form>
		</div>
	</nav>
	
	<div class="vertical-nav " id="sidebar">
		<div class="py-4 px-3 mb-4 " style="background-color: #114e60">


			<div class="bg-dark"></div>
			<div class="sidenav" style="display: inline-block;">
				<div class="vertical-nav bg-dark" id="sidebar">
					<div class="py-4 px-3 mb-4 " style="background-color: #114e60">
						<div class="media d-flex align-items-center">
							<div class="media-body">
								<h2 class="m-0 text-center text-gray font-weight-bold">LOGO</h2>								
							</div>
						</div>
					</div>

					<p
						class="text-gray font-weight-bold text-uppercase px-3 small pb-4 mb-0">Main</p>

					<ul class="nav flex-column bg-dark mb-0">
						<li class="nav-item"><a href="./showAdd"
							class="nav-link text-white  bg-dark "> <i
								class="fas fa-qrcode  mr-3 text-primary fa-fw"></i> Material
								Group Creation
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fa fa-address-card mr-3 text-primary fa-fw"></i> Material
								Make Creation
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fa fa-cubes mr-3 text-primary fa-fw disable"></i>
								Material Assembly Creation
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fa fa-address-card mr-3 text-primary fa-fw"></i> Unit of
								Measurement
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fas fa-qrcode  mr-3 text-primary fa-fw"></i> Material
								Master
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fas fa-qrcode  mr-3 text-primary fa-fw"></i>
								Vendor/Supplier Creation
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fa fa-address-card mr-3 text-primary fa-fw"></i> Standard
								Rate Capture
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fa fa-cubes mr-3 text-primary fa-fw disable"></i> Store
								Creation
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fas fa-qrcode  mr-3 text-primary fa-fw"></i> Purchase
								indent
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fas fa-qrcode  mr-3 text-primary fa-fw"></i> Publish
								Purchase indent
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fa fa-address-card mr-3 text-primary fa-fw"></i> Creation
								of Purchase Order
						</a></li>
						<li class="nav-item"><a href="#"
							class="nav-link text-white  bg-dark"> <i
								class="fa fa-cubes mr-3 text-primary fa-fw disable"></i> Receipt
								of material
						</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>



</body>